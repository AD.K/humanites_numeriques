<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xhtml"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>


    <xsl:template match="TEI">
        <html style="padding-left:50pt">
            <head>
                <h1 style="color:blue">
                    <xsl:value-of select="teiHeader/fileDesc/titleStmt/title"/>
                </h1>
            </head>
            <body>
                <xsl:apply-templates select="text"/>
                <div/>
            </body>
        </html>
        
        <xsl:result-document href="mon-ead.xml" doctype-public="+//ISBN 1-931666-00-8//DTD ead.dtd (Encoded Archival Description (EAD) Version 2002)//EN" doctype-system="ead.dtd">
            <ead>  
                <eadheader>
                    <eadid  countrycode="FR"></eadid>
                    <filedesc>
                        <titlestmt>
                            <titleproper>
                                
                                <xsl:value-of select="//fileDesc/sourceDesc/msDesc/history/origin"/>
                            </titleproper>
                        </titlestmt>
                    </filedesc>
                </eadheader>          
                
                <archdesc level="item"> 
                    <did>
                        <origination>
                            <xsl:value-of select="//fileDesc/sourceDesc/msDesc/msIdentifier"/>
                        </origination>
                        
                        </did>
                    
                    <dsc>
                        <c>
                            <did>
                                <physdesc>
                                    <xsl:value-of select="//fileDesc/sourceDesc/msDesc/physDesc"/> 
                                </physdesc>
                                
                            </did>
                        </c>
                    </dsc>      
                </archdesc>            
            </ead>   
            
        </xsl:result-document>
        
                
        </xsl:template>

    <xsl:template match="front">

        <div style="color:red">
            <h3>A-Corpus</h3>
            <a href="couverture1.jpg">couverture1.jpg</a>
            <p>
                <a href="couverture2.jpg">couverture2.jpg</a>
            </p>
            <p>
                <a href="couverture3.jpg">couverture3.jpg</a>
            </p>
            <p>
                <a href="MS1..jpg">MS1..jpg</a>
            </p>
            <p>
                <a href="MS2.jpg">MS2.jpg</a>
            </p>
            <p>
                <a href="MS3.jpg">MS3.jpg</a>
            </p>
            <p>
                <a href="MS4.jpg">MS4.jpg</a>
            </p>
            <p>
                <a href="MS5.jpg">MS5.jpg</a>
            </p>
            <p>
                <a href="MS6.jpg">MS6.jpg</a>
            </p>
            <p>
                <a href="MS7.jpg">MS7.jpg</a>
            </p>


        </div>
        <div>
            <h3 style="color:red">
                <xsl:value-of select="//front/head"/>
            </h3>
            <h6>a- Manuscript</h6>
            <p>
                <xsl:value-of select="//front/div[@facs = '#manuscript']/p/date"/>
            </p>
            <p>
                <xsl:value-of select="//front/div/p/title"/>
            </p>

        </div>

        <p>
            <xsl:value-of select="/front/div/p/title/p"/>
        </p>
        <div>
            <p>
                <xsl:value-of select="//front/div/div/p"/>
            </p>
            <p>
                <h6>
                    <xsl:value-of select="//front/div[@facs = '#page-de-titre']/head"/>
                </h6>
            </p>
            <p>
                <xsl:value-of select="//front/div[@facs = '#page-de-titre']/p"/>
            </p>
        </div>

    </xsl:template>

    <xsl:template match="body">

        <div>
            <h3 style="color:red">C- Tapuscript</h3>
            <h5>Page2</h5>
            <p>

                <xsl:value-of select="//body/div[@facs = '#tapuscript']/p[@style = 'saisie1']"/>
            </p>
            <p>
                <xsl:value-of select="//body/div[@facs = '#tapuscript']/p[@style = 'saisie2']"/>
            </p>
            <p>
                <xsl:value-of select="//body/div[@facs = '#tapuscript']/p[@style = 'saisie3']"/>
            </p>
            <p>
                <xsl:value-of select="//body/div[@facs = '#tapuscript']/p[@style = 'saisie4']"/>
            </p>

        </div>
        <div>
            <h5>Page3</h5>
            <p>
                <xsl:value-of select="//body/div[@facs = '#tapuscript']/p[@style = 'p2']"/>
            </p>
        </div>
        <div>
            <h5>Page4</h5>
            <p>
                <xsl:value-of select="//body/div[@facs = '#tapuscript']/p[@style = 'p4']"/>
            </p>
        </div>
        <div>
            <h5>Page5</h5>
            <p>
                <xsl:value-of select="//body/div[@facs = '#tapuscript']/p[@style = 'p5']"/>
            </p>
        </div>
        <h5>Page6</h5>

        <div>
            <h3 style="color:red">D- Métadonnées</h3>
            <h5>a- Auteur</h5>
            <xsl:value-of select="//fileDesc/titleStmt/author"/>
            <h5>b- Editeur</h5>
            <xsl:value-of select="//fileDesc/titleStmt/editor"/>
            <h5>c- Institution</h5>
            <xsl:value-of select="//fileDesc/publicationStmt/authority"/>
            <h5> d- Licence</h5>
            <xsl:value-of select="//fileDesc/publicationStmt/availability/licence"/>
            <h5>e- Source</h5>
            <xsl:value-of select="//fileDesc/sourceDesc/msDesc/msIdentifier"/>
            <h5>f- Etat physique</h5>
            <xsl:value-of select="//fileDesc/sourceDesc/msDesc/physDesc"/>
            <h5>g- Histoire et origine</h5>
            <xsl:value-of select="//fileDesc/sourceDesc/msDesc/history/origin"/>
            <h5>h- LIEN POUR CONSULTATION DE LA SOURCE</h5>
            <a
                href="https://mondesfrancophones.com/mondes-caribeens/cahier-dun-retour-au-pays-natal-manuscrit-tapuscrit-inedit-presente-par-rene-henane"
                >
                https://mondesfrancophones.com/mondes-caribeens/cahier-dun-retour-au-pays-natal-manuscrit-tapuscrit-inedit-presente-par-rene-henane </a>
            <p/>
        </div>

    </xsl:template>


</xsl:stylesheet>
